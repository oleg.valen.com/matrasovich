from django.conf import settings
from django.core.paginator import Paginator
from django.db import models
from django.db.models import Model
from django.utils import timezone
from django.urls import reverse


# Create your models here.
class CategoryQuerySet(models.QuerySet):
    def active(self):
        return self.filter(active=True)


class Category(models.Model):
    name = models.CharField(max_length=128)
    sort_order = models.PositiveIntegerField(blank=True, default=0)
    active = models.BooleanField(default=False)
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)
    slug = models.CharField(max_length=256, unique=True)

    objects = CategoryQuerySet.as_manager()

    class Meta:
        verbose_name_plural = 'Categories'
        ordering = ['sort_order']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.updated = timezone.now()
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        kwargs = {'slug': self.slug, 'category_id': self.pk}
        return reverse('category:index', kwargs=kwargs)

    @staticmethod
    def get_product_list(queryset, request):
        """
        Return data for paginate a page depending on the setting 'page size'.

        :param queryset: product queryset
        :param request: request
        :return: tuple
        """
        paginator = Paginator(queryset, settings.PAGE_SIZE)
        product_paginator = paginator.page(request.GET.get('page', 1))
        product_list = product_paginator.object_list
        return paginator, product_paginator, product_list
