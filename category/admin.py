from django.contrib import admin

from category.models import Category


# Register your models here.
class CategoryAdmin(admin.ModelAdmin):
    fields = ('name', 'slug', 'active', 'sort_order', 'created', 'updated')
    list_display = ('name', 'active')
    search_fields = ['name']
    readonly_fields = ('created', 'updated')

admin.site.register(Category, CategoryAdmin)
