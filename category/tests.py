from django.test import TestCase, RequestFactory

# Create your tests here.
from django.urls import reverse

from attribute.models import CategoryAttribute, NOINDEX_NOFOLLOW, INDEX_FOLLOW
from category.models import Category
from product.models import Product


class CategoryMixin:
    fixtures = ['attribute', 'category', 'product']

    def setUp(self):
        self.factory = RequestFactory()
        self.category = Category.objects.get(pk=1)
        self.products = Product.objects.filter(pk__in=[1, 2])
        self.category_attribute = CategoryAttribute.objects.get(pk=2)


class CategoryTest(CategoryMixin, TestCase):

    def test_active(self):
        self.assertQuerysetEqual(Category.objects.active(), [True], transform=lambda category: category.active)

    def test_get_absolue_url(self):
        self.assertEqual('/toppery/c1/', self.category.get_absolute_url())


class CategoryIndexTest(CategoryMixin, TestCase):

    def test_get_category_attribute(self):
        self.assertTupleEqual((CategoryAttribute.get_category_attribute(self.category)),
                              (None, None, self.category_attribute))

    def test_product_active(self):
        self.assertQuerysetEqual(Product.objects.active(), [True] * 5, transform=lambda product: product.active)

    def test_product_sort(self):
        kwargs = {'slug': self.category.slug, 'category_id': self.category.pk}

        request = self.factory.get('{}?sort=cheap'.format(reverse('category:index', kwargs=kwargs)))
        qs = self.category.products.sort(request)
        self.assertQuerysetEqual(qs, [1, 2, 3, 4, 5], transform=lambda product: product.pk)

        request = self.factory.get('{}?sort=expensive'.format(reverse('category:index', kwargs=kwargs)))
        qs = self.category.products.sort(request)
        self.assertQuerysetEqual(qs, [5, 4, 2, 3, 1], transform=lambda product: product.pk)

    def test_attribute_groups(self):
        self.assertQuerysetEqual(self.products.attribute_groups(), [1, 2, 3], transform=lambda ag: ag.pk)

    def test_review_images(self):
        self.assertDictEqual(self.products.review_images(), {1: 0, 2: 0})

    def test_get_attribute_groups(self):
        attribute_groups_qs = self.products.attribute_groups()
        attribute_groups = attribute_groups_qs.get_attribute_groups(self.category.get_absolute_url())
        dict = {
            'pk': 1,
            'name': 'Недорогие',
            'slug': 'nedorogie',
            'url': '/toppery/c1/category=nedorogie/',
            'checked': ''
        }

        self.assertDictEqual(dict, attribute_groups['Категория'][1])
        self.assertNotIn('2', attribute_groups['Категория'])

    def test_stickers(self):
        stickers = self.products.stickers()
        self.assertCountEqual(list(stickers.keys()), [1, 2])
        self.assertEqual(list(stickers[1].keys()), [1, 2, 4])

    def test_reviews(self):
        reviews = self.products.reviews()
        self.assertDictEqual({1: {'count': 3, 'avg': 4.7}, 2: {'count': 2, 'avg': 4.0}}, reviews)

    def test_get_robots(self):
        self.assertEqual(CategoryAttribute.get_robots(None, None), NOINDEX_NOFOLLOW)
        self.assertEqual(CategoryAttribute.get_robots(self.category_attribute, None), INDEX_FOLLOW)



