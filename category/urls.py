from django.urls import path, register_converter
from . import views, converters

app_name = 'category'
register_converter(converters.FilterSlugConverter, 'filter')
urlpatterns = [
    path('<slug:slug>/c<int:category_id>/', views.index, name='index'),
    path('<slug:slug>/c<int:category_id>/<filter:filter>/', views.category_filter, name='category_filter'),
]
