from django.shortcuts import get_object_or_404
from django.http.response import JsonResponse

from category.models import Category
from django.conf import settings

from common.views import get_model, get_breadcrumbs, get_pagination
from attribute.models import AttributeGroup, CategoryAttribute, ATTRIBUTE_GROUP_SIZE


# Create your views here.
def index(request, slug, category_id):
    """
    Return json for a category_id.
    Url: /toppery/c2/

    :param slug: slug of the category (use only for creating url)
    :param category_id: category id
    :return: json
    """
    category = get_object_or_404(Category, pk=category_id, active=True)
    category_url = category.get_absolute_url()
    _, _, category_attribute = CategoryAttribute.get_category_attribute(category)

    paginator, product_paginator, product_list = category.get_product_list(
        category.products.active().sort(request).all(), request)

    attribute_groups = product_list.attribute_groups()

    review_images = product_list.review_images()

    data = get_model(category)
    breadcrumbs = get_breadcrumbs([category])
    pagination = get_pagination(paginator, product_paginator)
    other = {
        'attribute_groups': attribute_groups.get_attribute_groups(category_url),
        'products': product_list.products(review_images),
        'stickers': product_list.stickers(),
        'reviews': product_list.reviews(),
        'meta_tags': CategoryAttribute.get_meta_tags(category_attribute, settings.META_FIELDS),
        'robots': CategoryAttribute.get_robots(category_attribute),
        'filter': {
            'has_one_size_selected': False,
            'size_name': '',
            'clear_urls': []
        }
    }
    data.update(**breadcrumbs, **pagination, **other)
    return JsonResponse(data)


def category_filter(request, slug, category_id, filter):
    """
    Return json for a category_id with other filters.
    Url: /toppery/c2/category=nedorogie,so-skidkoy;size=100x190/

    :param slug: slug of the category (use only for creating url)
    :param category_id: category id
    :param filter: for example: /category=nedorogie,so-skidkoy;size=100x190/
    Static url to be able to index (not get params)
    :return: json
    """

    category = get_object_or_404(Category, pk=category_id, active=True)
    category_url = category.get_absolute_url()
    filter_parsed, attribute_list, category_attribute = CategoryAttribute.get_category_attribute(category, filter)

    paginator, product_paginator, product_list = category.get_product_list(
        category.products.active().filtered(attribute_list).sort(request).all(), request)

    attribute_group_list = product_list.attribute_groups()
    attribute_groups = attribute_group_list.get_attribute_groups(category_url, filter_parsed)

    attribute_size_list = attribute_list.filtered_attribute_group(ATTRIBUTE_GROUP_SIZE)
    one_size_selected = attribute_size_list.count() == 1
    if one_size_selected:
        attribute_size = attribute_size_list.get()
        attribute_size_name = attribute_size.name
        prices = product_list.attribute_prices(attribute_size)
    else:
        attribute_size_name = ''

    review_images = product_list.review_images()

    products = product_list.products(review_images,
                                     prices=prices,
                                     attribute_group_name=ATTRIBUTE_GROUP_SIZE,
                                     attribute_name=attribute_size_name)

    data = get_model(category)
    breadcrumbs = get_breadcrumbs([[category]])
    pagination = get_pagination(paginator, product_paginator)
    other = {
        'attribute_groups': attribute_groups,
        'products': products,
        'stickers': product_list.stickers(),
        'reviews': product_list.reviews(),
        'meta_tags': CategoryAttribute.get_meta_tags(category_attribute, settings.META_FIELDS,
                                                     category=category,
                                                     attribute_group_list=attribute_group_list,
                                                     filter_parsed=filter_parsed),
        'robots': CategoryAttribute.get_robots(category_attribute, attribute_list),
        'filter': {
            'has_one_size_selected': one_size_selected,
            'size_name': attribute_size_name,
            'clear_urls': AttributeGroup.get_clear_filter_urls(category_url, attribute_groups)
        }
    }
    data.update(**breadcrumbs, **pagination, **other)
    return JsonResponse(data)
