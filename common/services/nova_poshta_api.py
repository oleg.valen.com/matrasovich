import os
from abc import ABC, abstractmethod
import json
import requests

from django.conf import settings
from django.http import JsonResponse

from common.exceptions import NovaPoshtaAPIError


class Context:
    def __init__(self, method):
        self._method = method

    @property
    def method(self):
        return self._method

    @method.setter
    def method(self, method):
        self._method = method

    def execute(self, request):
        """
        The only entrypoint for Nova Poshta's API requests.

        :param request: request
        :return: if success, returns Nova Poshta data of the request
        """
        try:
            params = json.loads(request.body.decode())
            query = self._method.get_query(params)
            res = requests.post(settings.NOVA_POSHTA_API_URL, json=query)
            json_res = res.json()
            if not json_res['success']:
                raise NovaPoshtaAPIError
            return JsonResponse({'success': True, 'data': self._method.get_data(json_res)})

        except NovaPoshtaAPIError:
            return JsonResponse({'success': False, 'error': json_res['errors'][0]})
        except KeyError as e:
            return JsonResponse({'success': False, 'error': 'Mapping key not found ({}).'.format(e.args[0])})
        except Exception as e:
            return JsonResponse({'success': False, 'error': str(e)})


class Method(ABC):
    @abstractmethod
    def get_query(self, params):
        pass

    def get_data(self, json_res):
        """
        Returns list of the data depending on the needed fields.

        :param json_res: json result from the API
        :return: list of the API's data
        """
        return [{field: i[field] for field in self._fields} for i in json_res['data']]


class AddressGetCities(Method):
    def __init__(self):
        self._fields = ('Description', 'Ref', 'SettlementTypeDescription', 'AreaDescription')

    def get_query(self, params):
        return {
            'apiKey': os.getenv('NOVA_POSHTA_API_KEY'),
            'modelName': 'Address',
            'calledMethod': 'getCities',
            'methodProperties': {
                'FindByString': params['city']
            }
        }


class AddressGeneralGetWarehouses(Method):
    def __init__(self):
        self._fields = ('Description', 'Ref')

    def get_query(self, params):
        return {
            'apiKey': os.getenv('NOVA_POSHTA_API_KEY'),
            'modelName': 'AddressGeneral',
            'calledMethod': 'getWarehouses',
            'methodProperties': {
                'CityRef': params['city_ref'],
                'TypeOfWarehouseRef': settings.NOVA_POSHTA_API_TYPE_OF_WAREHOUSE_REF
            }
        }


class AddressGetStreets(Method):
    def __init__(self):
        self._fields = ('StreetsType', 'Description', 'Ref')

    def get_query(self, params):
        return {
            'apiKey': os.getenv('NOVA_POSHTA_API_KEY'),
            'modelName': 'Address',
            'calledMethod': 'getStreet',
            'methodProperties': {
                'CityRef': params['city_ref'],
                'FindByString': params['street']
            }
        }
