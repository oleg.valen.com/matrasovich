def get_model(model):
    """
    Returns main model data as a part of the json response.

    :param model: model
    :return: dict
    """
    return {
        'model': {
            'name': model.name,
            'id': model.id,
            'base_url': model.get_absolute_url(),
        }
    }


def get_breadcrumbs(list):
    # todo урл главной страницы
    """
    Returns breadcrumbs of a list as a part of the json response.

    :param list: list
    :return: dict
    """
    return {'breadcrumbs': [{'name': 'Главная', 'url': 'url'}] + [{'name': l.name, 'url': l.get_absolute_url()} for l in
                                                                  list]}


def get_pagination(paginator, model_paginator):
    """
    Returns pagination data as a part of the json response.

    :param paginator: paginator
    :param model_paginator: model_paginator
    :return: dict
    """
    return {
        'pagination': {
            'count': paginator.count,
            'num_pages': paginator.num_pages,
            'per_page': paginator.per_page,
            'has_next': model_paginator.has_next(),
            'has_previous': model_paginator.has_previous(),
        }
    }
