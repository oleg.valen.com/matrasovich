from django.db.models.query import QuerySet, EmptyQuerySet
from django.db.models import Model


def get_dict(qs_model, fields, default=''):
    """
    Retrieves fields from a queryset or model.

    :param qs_model: queryset or model
    :param fields: needed fields
    :param default: default value
    :return: dict if model, list of the data if queryset
    """
    query = dict.fromkeys(fields, default)
    if isinstance(qs_model, EmptyQuerySet):
        return query
    if isinstance(qs_model, Model):
        return {f: getattr(qs_model, f, default) for f in fields}
    elif isinstance(qs_model, QuerySet):
        return [{f: getattr(q, f, default) for f in fields} for q in qs_model]
