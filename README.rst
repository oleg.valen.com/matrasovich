matrasovich source code
=============================

To run locally, do the usual:
-----------------

#. Create a Python 3.8 virtualenv::

    python3 -m venv env
    source env/bin/activate

#. Install dependencies::

    pip install -r requirements/dev.txt
    npm install

#. Create tables::

    ./manage.py migrate

#. Create a superuser::

   ./manage.py createsuperuser

#. Populate tables from fixtures::

    ./manage.py loaddata category
    ./manage.py loaddata attribute
    ./manage.py loaddata product
    ./manage.py loaddata order
    ./manage.py loaddata serie

#. Set up environment variable 'NOVA_POSHTA_API_KEY' for testing and working with Nova Poshta API.

#. Run the server.

Running the tests
-----------------

Run tests::

    ./manage.py test category
    ./manage.py test order
    ./manage.py test product
    ./manage.py test serie
