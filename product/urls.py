from django.urls import path
from . import views

app_name = 'product'
urlpatterns = [
    path('<slug:slug>/p<int:product_id>/', views.index, name='index'),
    path('product/detail-info/', views.detail_info, name='detail_info'),

]
