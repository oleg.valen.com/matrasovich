import json

from django.http.response import JsonResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from common.utils import get_dict
from django.conf import settings

from common.views import get_breadcrumbs, get_model
from product.models import Product, Detail
from attribute.models import Attribute, ATTRIBUTE_GROUP_SIZE, INDEX_FOLLOW


# Create your views here.
def index(request, slug, product_id):
    """
    Return json for a product_id.
    Url: /matras-topper-sleep-fly-mini-flex-mini-zhakkard/p1/
    Url: /matras-topper-sleep-fly-mini-flex-mini-zhakkard/p1/?size=100x190/

    :param slug: slug of the product (use only for creating url)
    :param product_id: product_id
    :return: json
    """

    try:
        product_detail = Product.objects.filter(pk=product_id).active()
    except Product.DoesNotExist:
        raise Http404('Could not find product with product_id {}'.format(product_id))
    product = product_detail.get()

    review_images = product_detail.review_images()

    analogues = product.analogues.all()
    analogues_review_images = analogues.review_images()

    attribute_size = None
    attribute_size_name = request.GET.get(ATTRIBUTE_GROUP_SIZE)
    if attribute_size_name is not None:
        attribute_size = Attribute.get_attribute(ATTRIBUTE_GROUP_SIZE, attribute_size_name)
        prices = product_detail.attribute_prices(attribute_size)
        analog_prices = analogues.attribute_prices(attribute_size)

    product_options = analogues_options = {}
    if attribute_size:
        product_options = {'prices': prices,
                           'get': '?',
                           'attribute_group_name': ATTRIBUTE_GROUP_SIZE,
                           'attribute_name': attribute_size_name}
        analogues_options = {'prices': analog_prices,
                             'attribute_group_name': ATTRIBUTE_GROUP_SIZE,
                             'attribute_name': attribute_size_name}

    data = get_model(product)
    breadcrumbs = get_breadcrumbs([product.category, product])
    other = {
        'product': product_detail.products(review_images, **product_options),
        'options': product.all_options(),
        'details': product.all_details(),
        'analogues': analogues.products(analogues_review_images, **analogues_options),
        'details_compared': (product_detail | analogues).details_compared(),
        'stickers': product_detail.stickers(),
        'reviews': product_detail.reviews(),
        'meta_tags': get_dict(product, settings.META_FIELDS),
        'robots': INDEX_FOLLOW,
    }
    data.update(**breadcrumbs, **other)
    return JsonResponse(data)


@require_POST
@csrf_exempt
def detail_info(request):
    """
    Return Detail model data.
    Body: product_id, detail_id
    Url: /product/detail-info/

    :return: name, text of Detail model
    """
    try:
        fields = ('product_id', 'detail_id')
        data = json.loads(request.body.decode())
        product_id, detail_id = map(lambda i: data.pop(i), fields)
        detail = Detail.detail_info(product_id, detail_id)
        return JsonResponse({'success': True, 'name': detail.name, 'text': detail.text})
    except KeyError as e:
        return JsonResponse({'success': False, 'error': 'Mapping key not found ({}).'.format(e.args[0])})
    except Exception as e:
        return JsonResponse({'success': False, 'error': str(e)})
