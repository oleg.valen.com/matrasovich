from decimal import Decimal

from django.test import TestCase, RequestFactory

# Create your tests here.
from django.urls import reverse

from attribute.models import CategoryAttribute, NOINDEX_NOFOLLOW, INDEX_FOLLOW, ATTRIBUTE_GROUP_SIZE, Attribute
from category.models import Category
from product.models import Product
from serie.models import Serie


class ProductMixin:
    fixtures = ['attribute', 'category', 'product', 'serie']

    def setUp(self):
        self.factory = RequestFactory()
        self.product = Product.objects.get(pk=1)
        self.product_detail = Product.objects.filter(pk=1)
        self.products = Product.objects.filter(pk__in=[1, 2])
        self.serie = Serie.objects.get(pk=1)
        self.category = Category.objects.get(pk=1)
        self.category_attribute = CategoryAttribute.objects.get(pk=2)


class ProductTest(ProductMixin, TestCase):

    def test_active(self):
        self.assertQuerysetEqual(Product.objects.active(), [True] * 5, transform=lambda product: product.active)

    def test_get_absolue_url(self):
        self.assertEqual('/matras-topper-sleep-fly-mini-flex-mini-zhakkard/p1/', self.product.get_absolute_url())


class ProductIndexTest(ProductMixin, TestCase):

    def test_attribute_size(self):
        kwargs = {'slug': self.product.slug, 'product_id': self.product.pk}
        request = self.factory.get(reverse('product:index', kwargs=kwargs), {'size': '100x190'})
        attribute_size_name = request.GET.get(ATTRIBUTE_GROUP_SIZE)
        if attribute_size_name is not None:
            attribute_size = Attribute.get_attribute(ATTRIBUTE_GROUP_SIZE, attribute_size_name)
            self.assertEqual(attribute_size.name, request.GET.get(ATTRIBUTE_GROUP_SIZE))
            self.assertEqual(self.product_detail.attribute_prices(attribute_size)[1], Decimal(1512.00))

    def test_products(self):
        dict = {1:
                    {'pk': 1,
                     'name': 'Матрас топпер Sleep&Fly Mini Flex Mini Жаккард',
                     'price': Decimal('1464.00'),
                     'slug': 'matras-topper-sleep-fly-mini-flex-mini-zhakkard',
                     'url': '/matras-topper-sleep-fly-mini-flex-mini-zhakkard/p1/',
                     'image': '',
                     'has_review_image': False}
                }
        self.assertDictEqual(dict, self.product_detail.products({1: 0}))

    def test_all_options(self):
        with self.assertRaises(AttributeError):
            self.product_detail.all_options()

        with self.assertRaises(AttributeError):
            self.products.all_options()

        options = self.product.all_options()
        is_not_attribute_size_id = 3
        is_attribute_size_id = 4
        self.assertIsNone(options.get(is_not_attribute_size_id))
        self.assertIsNotNone(options.get(is_attribute_size_id))

    def test_all_details(self):
        details = self.product.all_details()
        is_group_serie_id = 1
        is_not_group_serie_id = 3
        self.assertEqual(list(details.keys()), [1, 3, 5])
        self.assertEqual(details[is_group_serie_id]['url'], self.serie.get_absolute_url())
        self.assertIs(details[is_not_group_serie_id]['url'], '')
