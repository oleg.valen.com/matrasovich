from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Count, Avg, Prefetch
from django.db.models.functions import Coalesce
from django.urls import reverse
from django.utils import timezone

from category.models import Category
from attribute.models import Attribute, AttributeGroup

STICKER_CLASS_NAME = [
    ('is-primary', 'is-primary'),
    ('is-link', 'is-link'),
    ('is-info', 'is-info'),
    ('is-success', 'is-success'),
    ('is-warning', 'is-warning'),
    ('is-danger', 'is-danger'),
]

DETAIL_GROUP_SERIE = 'Серия'


# Create your models here.
class ProductQuerySet(models.QuerySet):
    def active(self):
        return self.filter(active=True)

    def sort(self, request):
        """
        Sorts queryset depending on the get paramemer 'sort'.

        :param request: request
        :return: queryset
        """
        sort = request.GET.get('sort')
        if sort == 'cheap':
            return self.order_by('price')
        elif sort == 'expensive':
            return self.order_by('-price')
        return self

    def filtered(self, attribute_list):
        """
        Gets filter data from an attribute queryset.

        :param attribute_list: attribute queryset
        :return: queryset
        """
        return self.filter(attributes__in=attribute_list).distinct()

    def attribute_groups(self):
        """
        Returns attribute groups an attributes of the product queryset.

        :return: queryset
        """
        return AttributeGroup.objects.prefetch_related(
            Prefetch('attrs', queryset=Attribute.objects.filter(product__in=self).distinct()))

    def stickers(self):
        """
        Returns dict of the product stickers.

        :return: dict
        """
        return {p.pk: {s.pk: {'pk': s.pk, 'name': s.name, 'class': s.class_name} for s in p.stickers.all()} for p in
                self.prefetch_related('stickers').all()}

    def reviews(self):
        """
        Returns dict of product reviews data: count and average.

        :return: dict
        """
        return {p.pk: {'count': p.reviews_count, 'avg': round(p.reviews_avg, 1)} for p in
                self.prefetch_related('reviews').annotate(
                    reviews_count=Coalesce(Count('reviews'), 0),
                    reviews_avg=Coalesce(Avg('reviews__rate'), 0.0))}

    def review_images(self):
        """
        Returns dict of product review images: product pk and count of review images.

        :return: dict
        """
        return dict(self.annotate(Count('review_images')).values_list('pk', 'review_images__count'))

    def attribute_prices(self, attribute):
        """
        Returns dict of product prices depending on the option (size of the product): product pk and price.

        :param attribute:
        :return: dict
        """
        return {i.product_id: i.price for i in Option.objects.filter(attribute=attribute, product__in=self)}

    def products(self, review_images, **kwargs):
        """
        Returns main data of the product.

        :param review_images: has review images by product pk or not
        :param kwargs: options for price and url
        :return: dict
        """

        def _url(product, **kwargs):
            """
            Returns product url (with get or without get parameter).

            :param product: product
            :param kwargs: kwargs
            :return: url
            """
            if not kwargs:
                return product.get_absolute_url()
            return '{url}{get}{group}={attr}'.format(
                url=product.get_absolute_url(),
                get=kwargs.get('get', ''),  # '?' or ''
                group=kwargs['attribute_group_name'],
                attr=kwargs['attribute_name'])

        return {p.pk: {
            'pk': p.pk,
            'name': p.name,
            'price': p.price if not kwargs else kwargs['prices'].get(p.pk, 0),
            'slug': p.slug,
            'url': _url(p, **kwargs),
            'image': p.product_images.earliest().image.url if p.product_images.exists() else '',
            'has_review_image': review_images.get(p.pk) > 0,
        } for p in self}

    def details_compared(self):
        """
        Returns dict of product detail data for comparing.

        :return: dict
        """
        result = {}
        for dg in DetailGroup.objects.prefetch_related(Prefetch('details__products', queryset=self)).all():
            dict = {}
            for d in dg.details.all():
                for p in d.products.all():
                    dict[p.pk] = d.name
                result[dg.name] = dict
        return result


class Sticker(models.Model):
    name = models.CharField(max_length=128)
    sort_order = models.PositiveIntegerField(blank=True, default=0)
    class_name = models.CharField(max_length=16, choices=STICKER_CLASS_NAME, blank=True)

    class Meta:
        ordering = ['sort_order']

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=128)
    sort_order = models.PositiveIntegerField(blank=True, default=0)
    active = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)
    slug = models.CharField(max_length=256, unique=True)
    text = models.TextField(verbose_name='Content', blank=True)
    title = models.CharField(max_length=256, blank=True)
    h1 = models.CharField(max_length=256, blank=True)
    description = models.CharField(max_length=256, blank=True)
    keyword = models.CharField(max_length=256, blank=True)
    category = models.ForeignKey(Category, related_name='products', null=True, on_delete=models.SET_NULL)
    attributes = models.ManyToManyField(Attribute, related_name='products', related_query_name='product', blank=True)
    analogues = models.ManyToManyField('self', related_name='+', symmetrical=False, blank=True)
    stickers = models.ManyToManyField(Sticker, related_name='products', related_query_name='product', blank=True)

    objects = ProductQuerySet.as_manager()

    class Meta:
        ordering = ['-sort_order']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.updated = timezone.now()
        super().save(*args, **kwargs)

    def get_absolute_url(self, **kwargs):
        kwargs.update({'slug': self.slug, 'product_id': self.pk})
        return reverse('product:index', kwargs=kwargs)

    def all_options(self):
        """
        Returns dict of size prices of the queryset.

        :return: dict
        """
        return {o.attribute_id: {'attribute_id': o.attribute_id, 'name': o.attribute.name, 'price': o.price} for o in
                self.options.all()}

    def all_details(self):
        """
        Returns dict of product detail of the queryset.

        :return: dict
        """
        return {d.id: {'id': d.id, 'group': d.detail_group.name, 'name': d.name, 'url': self._serie_url(d)} for d in
                self.details.all().select_related('detail_group')}

    def _serie_url(self, detail):
        """
        Returns serie url of the detail or ''.

        :param detail: detail model
        :return: serie url
        """
        if detail.detail_group.name != DETAIL_GROUP_SERIE:
            return ''
        try:
            return detail.series.get().get_absolute_url()
        except ObjectDoesNotExist:
            return ''


class DetailGroup(models.Model):
    name = models.CharField(max_length=128)
    sort_order = models.PositiveIntegerField(blank=True, default=0)

    class Meta:
        ordering = ['sort_order']

    def __str__(self):
        return self.name


class Detail(models.Model):
    name = models.CharField(max_length=128)
    sort_order = models.PositiveIntegerField(blank=True, default=0)
    text = models.TextField(verbose_name='Content', blank=True)
    detail_group = models.ForeignKey(DetailGroup, related_name='details', related_query_name='detail', null=True,
                                     on_delete=models.SET_NULL)
    products = models.ManyToManyField(Product, related_name='details', related_query_name='detail')

    class Meta:
        ordering = ['sort_order']

    def __str__(self):
        return self.name

    @staticmethod
    def detail_info(product_id, detail_id):
        """
        Returns detail model.

        :param product_id: product_id
        :param detail_id: detail_id
        :return: detail model
        """
        return Detail.objects.get(pk=detail_id, products__pk=product_id)


class Option(models.Model):
    price = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    product = models.ForeignKey(Product, related_name='options', null=True, on_delete=models.SET_NULL)
    attribute = models.ForeignKey(Attribute, related_name='options', null=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ['price']

    def __str__(self):
        return '{}, {}'.format(self.product.name, self.attribute.name)


class Review(models.Model):
    name = models.CharField(max_length=128)
    active = models.BooleanField(default=False)
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)
    text = models.TextField(verbose_name='Review', blank=True)
    rate = models.PositiveSmallIntegerField(choices=list((x, x) for x in range(1, 6)), blank=False)
    product = models.ForeignKey(Product, related_name='reviews', null=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return '{}, {}'.format(self.name, self.created.strftime('%d:%m:%Y %H:%M:%S'))

    def save(self, *args, **kwargs):
        self.updated = timezone.now()
        super().save(*args, **kwargs)


class ProductImage(models.Model):
    sort_order = models.PositiveIntegerField(blank=True, default=0)
    image = models.ImageField(upload_to='product/static/product/images/')
    product = models.ForeignKey(Product, related_name='product_images', on_delete=models.DO_NOTHING)

    class Meta:
        ordering = ['sort_order']
        get_latest_by = ['sort_order']

    def __str__(self):
        return self.image.name


class ReviewImage(models.Model):
    sort_order = models.PositiveIntegerField(blank=True, default=0)
    image = models.ImageField(upload_to='product/static/review/images/')
    product = models.ForeignKey(Product, related_name='review_images', on_delete=models.DO_NOTHING)

    class Meta:
        ordering = ['sort_order']

    def __str__(self):
        return self.image.name
