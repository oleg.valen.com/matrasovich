from django.contrib import admin

from product.models import Sticker, Product, DetailGroup, Detail, Option, Review, ProductImage, ReviewImage


# Register your models here.
class OptionInline(admin.TabularInline):
    model = Option
    # todo select attribute only from 'size' attribute group
    fields = ('attribute', 'price')


class ReviewInline(admin.TabularInline):
    model = Review
    readonly_fields = ('get_created',)
    fields = ('name', 'active', 'rate', 'text', 'get_created')

    @admin.display(description='Created')
    def get_created(self, obj):
        return obj.created.strftime('%d:%m:%Y %H:%M')


class ProductImageInline(admin.TabularInline):
    model = ProductImage
    ordering = ['sort_order']
    fields = ('image', 'sort_order')
    # todo show image (also in review)


class ReviewImageInline(admin.TabularInline):
    model = ReviewImage
    ordering = ['sort_order']
    fields = ('image', 'sort_order')


# class ProductInline(admin.TabularInline):
#     model = Product.stickers.through


class StickerAdmin(admin.ModelAdmin):
    pass


class ProductAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name', 'category', 'slug', 'price', 'active', 'sort_order', 'text', 'created', 'updated')
        }),
        ('Meta tags', {
            'fields': ('title', 'h1', 'description', 'keyword')
        }),
        (None, {
            'fields': ('attributes', 'analogues', 'stickers')
        })
    )
    filter_horizontal = ('analogues',)
    list_display = ('name', 'active')
    list_filter = ('category',)
    search_fields = ['name']
    readonly_fields = ('created', 'updated')
    inlines = [OptionInline, ProductImageInline, ReviewImageInline, ReviewInline]
    # todo WYSIWYG editor for text fields all over the base


class DetailGroupAdmin(admin.ModelAdmin):
    pass


class DetailAdmin(admin.ModelAdmin):
    fields = ('name', 'detail_group', 'text', 'sort_order', 'products')
    filter_horizontal = ('products',)
    list_display = ('name', 'detail_group')
    list_filter = ('detail_group',)


class ReviewAdmin(admin.ModelAdmin):
    readonly_fields = ('product', 'get_created', 'get_updated')
    fields = ('name', 'active', 'rate', 'product', 'text', 'get_created', 'get_updated')
    list_display = ('name', 'active', 'rate', 'product', 'text', 'get_created')
    list_filter = ('rate',)
    radio_fields = {'rate': admin.HORIZONTAL}
    ordering = ['-created']

    @admin.display(description='Created')
    def get_created(self, obj):
        return obj.created.strftime('%d:%m:%Y %H:%M')

    @admin.display(description='Updated')
    def get_updated(self, obj):
        return obj.updated.strftime('%d:%m:%Y %H:%M')


admin.site.register(Sticker, StickerAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(DetailGroup, DetailGroupAdmin)
admin.site.register(Detail, DetailAdmin)
admin.site.register(Review, ReviewAdmin)
