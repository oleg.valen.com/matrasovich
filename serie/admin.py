from django.contrib import admin

from serie.models import Serie


# Register your models here.
class SerieAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name', 'slug', 'active', 'sort_order', 'text', 'created', 'updated')
        }),
        ('Meta tags', {
            'fields': ('title', 'h1', 'description', 'keyword')
        }),
        (None, {
            'fields': ('detail',)
        })
    )
    list_display = ('name', 'active')
    readonly_fields = ('created', 'updated')



admin.site.register(Serie, SerieAdmin)
