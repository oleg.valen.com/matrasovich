from django.urls import path
from . import views

app_name = 'serie'
urlpatterns = [
    path('<slug:slug>/s<int:serie_id>/', views.index, name='index'),
]
