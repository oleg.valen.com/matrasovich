from django.shortcuts import get_object_or_404
from django.http.response import JsonResponse

from common.utils import get_dict
from django.conf import settings

from common.views import get_model, get_breadcrumbs
from attribute.models import INDEX_FOLLOW
from serie.models import Serie


# Create your views here.
def index(request, slug, serie_id):
    # /series-sleep-fly-mini/s1/

    serie = get_object_or_404(Serie, pk=serie_id, active=True)
    serie_url = serie.get_absolute_url()

    product_list = serie.products()
    review_images = product_list.review_images()

    data = get_model(serie)
    breadcrumbs = get_breadcrumbs([serie])
    other = {
        'products': product_list.products(review_images),
        'details_compared': product_list.details_compared(),
        'meta_tags': get_dict(serie, settings.META_FIELDS),
        'robots': INDEX_FOLLOW,
    }
    data.update(**breadcrumbs, **other)
    return JsonResponse(data)
