from django.test import TestCase

from serie.models import Serie


# Create your tests here.
class SerieMixin:
    fixtures = ['category', 'attribute', 'product', 'serie']

    def setUp(self):
        self.serie = Serie.objects.get(pk=1)


class SerieTest(SerieMixin, TestCase):

    def test_get_absolue_url(self):
        self.assertEqual('/series-sleep-fly-mini/s1/', self.serie.get_absolute_url())
