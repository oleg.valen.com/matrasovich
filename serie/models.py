from django.db import models
from django.urls import reverse
from django.utils import timezone

from product.models import Detail, Product


# Create your models here.
class SerieQuerySet(models.QuerySet):
    def active(self):
        return self.filter(active=True)


class Serie(models.Model):
    name = models.CharField(max_length=128)
    sort_order = models.PositiveIntegerField(blank=True, default=0)
    active = models.BooleanField(default=False)
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)
    slug = models.CharField(max_length=256, unique=True)
    text = models.TextField(verbose_name='Content', blank=True)
    title = models.CharField(max_length=256, blank=True)
    h1 = models.CharField(max_length=256, blank=True)
    description = models.CharField(max_length=256, blank=True)
    keyword = models.CharField(max_length=256, blank=True)
    detail = models.ForeignKey(Detail, related_name='series', null=True, on_delete=models.SET_NULL)

    objects = SerieQuerySet.as_manager()

    class Meta:
        ordering = ['sort_order']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.updated = timezone.now()
        super().save(*args, **kwargs)

    def get_absolute_url(self, **kwargs):
        kwargs.update({'slug': self.slug, 'serie_id': self.pk})
        return reverse('serie:index', kwargs=kwargs)

    def products(self):
        product_ids = Detail.objects.get(pk=self.detail.pk).products.values_list('id', flat=True)
        return Product.objects.filter(pk__in=product_ids)

