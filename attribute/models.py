import copy
import operator
from functools import reduce

from django.db import models
from django.db.models import Q

from category.models import Category
from common.utils import get_dict

ATTRIBUTE_GROUP_SIZE = 'size'

INDEX_FOLLOW = 'index, follow'
INDEX_NOFOLLOW = 'index, nofollow'
NOINDEX_FOLLOW = 'noindex, follow'
NOINDEX_NOFOLLOW = 'noindex, nofollow'
ROBOTS = [
    (INDEX_FOLLOW, 'index, follow'),
    (INDEX_NOFOLLOW, 'index, nofollow'),
    (NOINDEX_FOLLOW, 'noindex, follow'),
    (NOINDEX_NOFOLLOW, 'noindex, nofollow'),
]


class AttributeGroupQuerySet(models.QuerySet):
    def get_attribute_groups(self, base_url, filter_parsed=None):
        """
        Get attribute groups and attributes for aside filter depending on the url.

        :param base_url: url of the category
        :param filter_parsed: parsed other parameters in the url
        :return: dict
        """
        _self = self._clone()
        return {ag.name: {a.pk: {
            'pk': a.pk,
            'name': a.name,
            'slug': a.slug,
            'url': self._get_filter_url(_self, base_url, ag.slug, a.slug, filter_parsed),
            'checked': self._get_checked(ag.slug, a.slug, filter_parsed),
            # 'rel': 'rel="nofollow"',  # todo rel attribute for urls
        } for a in ag.attrs.all()} for ag in self}

    @staticmethod
    def _get_checked(ag_slug, a_slug, filter_parsed):
        """
        Return html tag for aside filter for aside filter checkbox.

        :param ag_slug: attribute group slug
        :param a_slug: attribute slug
        :param filter_parsed: parsed other parameters in the url
        :return: ' checked' or ''
        """
        if not filter_parsed:
            return ''
        return ' checked' if filter_parsed.get(ag_slug, []).count(a_slug) > 0 else ''

    @staticmethod
    def _get_filter_url(_self, base_url, ag_slug, a_slug, filter_parsed):
        """
        Return the url of the aside filter checkbox depending on the url.
        If html tag is ' checked' then delete the current filter.
        If html tag is '' thed add the current filter.

        :param base_url: url of the category
        :param ag_slug: attribute group slug
        :param a_slug: attribute slug
        :param filter_parsed: parsed other parameters in the url
        :return: url
        """
        _filter = copy.deepcopy(filter_parsed)
        if not _filter:
            return '{}{}={}/'.format(base_url, ag_slug, a_slug)
        if _filter.get(ag_slug, []).count(a_slug) > 0:
            _filter[ag_slug].remove(a_slug)
            if len(_filter[ag_slug]) == 0:
                _filter.pop(ag_slug)
        else:
            if _filter.get(ag_slug) is None:
                _filter[ag_slug] = [a_slug]
            else:
                _filter[ag_slug].append(a_slug)

        filter_new_raw = {ag.slug: [a.slug for a in ag.attrs.all() if _filter.get(ag.slug, []).count(a.slug) > 0] for ag
                          in _self.all()}
        filter_new = {k: v for k, v in filter_new_raw.items() if len(v) > 0}
        if not filter_new:
            return base_url
        else:
            return '{}{}/'.format(base_url, ';'.join(k + '=' + ','.join(s for s in v) for k, v in filter_new.items()))

    def filtered_attribute_group(self, slug):
        """
        Return queryset by the slug of the attribute group.

        :param slug: slug of the attribute group
        :return: queryset
        """
        return self.filter(attribute_group__slug=slug)


# Create your models here.
class AttributeGroup(models.Model):
    name = models.CharField(max_length=128)
    sort_order = models.PositiveIntegerField(blank=True, default=0)
    slug = models.SlugField(max_length=256, unique=True)
    title = models.CharField(max_length=256, blank=True)
    h1 = models.CharField(max_length=256, blank=True)
    description = models.CharField(max_length=256, blank=True)
    keyword = models.CharField(max_length=256, blank=True)
    robots = models.CharField(max_length=17, choices=ROBOTS, blank=True, default=INDEX_FOLLOW)
    categories = models.ManyToManyField(Category, related_name='attr_groups', related_query_name='attr_group', blank=True)

    objects = AttributeGroupQuerySet.as_manager()

    class Meta:
        ordering = ['sort_order']

    def __str__(self):
        return self.name

    @staticmethod
    def get_clear_filter_urls(category_url, attribute_groups):
        """
        For buttons for cleaning aside filter

        :param category_url: base url of the category
        :param attribute_groups: attribute groups to delete
        :return: list of the buttons for clean filter
        """
        list = [['Очистить', category_url]]
        return list + [[a['name'], a['url']] for _, ag in attribute_groups.items() for _, a in ag.items() if
                       a['checked'] == ' checked']


class Attribute(models.Model):
    name = models.CharField(max_length=128)
    sort_order = models.PositiveIntegerField(blank=True, default=0)
    slug = models.SlugField(max_length=256)
    title = models.CharField(max_length=256, blank=True)
    h1 = models.CharField(max_length=256, blank=True)
    description = models.CharField(max_length=256, blank=True)
    keyword = models.CharField(max_length=256, blank=True)
    attribute_group = models.ForeignKey(AttributeGroup, related_name='attrs', related_query_name='attr', null=True,
                                        on_delete=models.SET_NULL)

    class Meta:
        ordering = ['attribute_group', 'sort_order']
        unique_together = [['slug', 'attribute_group']]

    def __str__(self):
        return self.name

    @staticmethod
    def get_attribute(ag_slug, a_slug):
        """
        Return attribute model by attribute group slug and attribute slug.

        :param ag_slug: attribute group slug
        :param a_slug: attribute slug
        :return: attribute model
        """
        try:
            return Attribute.objects.get(attribute_group__slug=ag_slug, slug=a_slug)
        except Attribute.DoesNotExist:
            return None


class CategoryAttribute(models.Model):
    text = models.TextField(verbose_name='Content', blank=True)
    title = models.CharField(max_length=256, blank=True)
    h1 = models.CharField(max_length=256, blank=True)
    description = models.CharField(max_length=256, blank=True)
    keyword = models.CharField(max_length=256, blank=True)
    category = models.ForeignKey(Category, related_name='category_attrs', null=True,
                                 on_delete=models.SET_NULL)
    attribute = models.ForeignKey(Attribute, related_name='category_attrs', null=True,
                                  on_delete=models.SET_NULL)

    def __str__(self):
        return self.h1

    @staticmethod
    def get_category_attribute(category, filter=None):
        """
        Get attribute list from the part of the url (only groups and attributes that
        has products, having groups and attributes from the part of the url (filter)).
        Also get category attribute, if attribute list length = 1 and needed category
        attribute exists.
        Category attribute is for getting meta tags.

        :param category: base category
        :param filter: part of the url concerning aside filter
        :return: tuple
        """
        if filter is None:
            return None, None, category.category_attrs.get(attribute__isnull=True)

        # category=nedorogie,so-skidkoy;size=100x190
        filter_parsed = {i.split('=')[0]: i.split('=')[1].split(',') for i in filter.split(';')}
        # {'category': ['nedorogie', 'so-skidkoy'], 'size': ['100x190']}

        q = [Q(attribute_group__slug=k, slug__in=v) for k, v in filter_parsed.items()]
        q_or = reduce(operator.or_, q)
        attribute_list = Attribute.objects.filter(q_or)

        if len(attribute_list) == 1:
            return filter_parsed, attribute_list, category.category_attrs.get(attribute__in=attribute_list)
        return filter_parsed, attribute_list, None

    @staticmethod
    def get_meta_tags(category_attribute, fields, **kwargs):
        """
        Get content and meta tags for the url: text, title, h1, description, keyword

        :param category_attribute: if exists - get meta tags from a model, if not - make compound fields
        :param fields: fields for meta tags from a model
        :param kwargs: options for building compound fields
        :return: dict
        """
        if category_attribute is not None:
            return get_dict(category_attribute, fields)

        text = title = h1 = description = keyword = ''
        for ag in kwargs['attribute_group_list'].all():
            for a in ag.attrs.all():
                if kwargs['filter_parsed'].get(ag.slug, []).count(a.slug) > 0:
                    title = ', '.join([title, a.name])
                    h1 = ', '.join([h1, a.h1])
                    description = ', '.join([description, a.description])
                    keyword = ', '.join([keyword, a.keyword])

        title, h1, description, keyword = map(
            lambda tag: '{} {}'.format(kwargs['category'].name, tag).capitalize(), (title, h1, description, keyword))

        return dict(zip(fields, [text, title, h1, description, keyword]))

    @staticmethod
    def get_robots(category_attribute, attribute_list=None):
        """
        Get robots meta tag.

        :param category_attribute:
        If None - category attribute is not exists.
        Else - getting robots meta tag directly from a model of the attribute group.
        :param attribute_list:
        If None - for main page and for category page, for example: /, /toppery/c1
        :return: robots meta tag
        """
        if category_attribute is None:
            return NOINDEX_NOFOLLOW
        if attribute_list is None:
            return INDEX_FOLLOW
        return AttributeGroup.objects.get(attr__in=attribute_list).robots
