from django.contrib import admin

from attribute.models import AttributeGroup, Attribute, CategoryAttribute


# Register your models here.
class AttributeInline(admin.TabularInline):
    model = Attribute
    ordering = ['sort_order']
    fields = ('name', 'slug', 'sort_order', 'title', 'h1', 'description', 'keyword')


class AttributeGroupAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name', 'slug', 'sort_order')
        }),
        ('Meta tags', {
            'fields': ('title', 'h1', 'description', 'keyword', 'robots')
        }),
        (None, {
            'fields': ('categories',)
        })
    )
    list_display = ('name', 'robots')
    inlines = [AttributeInline]


class CategoryAttributeAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('text',)
        }),
        ('Meta tags', {
            'fields': ('title', 'h1', 'description', 'keyword')
        }),
        (None, {
            'fields': ('category', 'attribute',)
        })
    )
    list_display = ('h1', 'category', 'attribute')


admin.site.register(AttributeGroup, AttributeGroupAdmin)
admin.site.register(CategoryAttribute, CategoryAttributeAdmin)
