from django.db import transaction
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from common.services.nova_poshta_api import Context, AddressGetCities, AddressGeneralGetWarehouses, AddressGetStreets
from order.models import Order


# Create your views here.
@require_POST
@csrf_exempt
@transaction.atomic
def make_order(request):
    """
    Return Nova Poshta API data of the streets.
    Url: /order/make-order/
    Body:
    {
      'name': 'Иванов И.И.',
      'phone': '06745678912',
      'items': [
        {
          'product_id': 1,
          'attribute_id': 4,
          'amount': 2,
          'price': 1464.00
        },
        {
          'product_id': 1,
          'attribute_id': 5,
          'amount': 3,
          'price': 1484.00
        }
      ]
    }

    :return: order's data.
    For example:
    id      created             sum
    1       06.05.2022 11:35    7380
    """

    return Order.order(request)


@require_POST
@csrf_exempt
def address_get_cities(request):
    """
    Return Nova Poshta API data of the city.
    Url: /order/get-cities/
    Body: city=дне

    :return: list of the cities.
    For example:
    Description     Ref                                     SettlementTypeDescription   AreaDescription
    Дніпрельстан    eb54475c-e5e4-11e9-b48a-005056b24375    село                        Запорізька
    """
    return Context(AddressGetCities()).execute(request)


@require_POST
@csrf_exempt
def address_general_get_warehouses(request):
    """
    Return Nova Poshta API data of the warehouses.
    Url: /order/get-warehouses/
    Body: city_ref=db5c88f0-391c-11dd-90d9-001a92567626

    :return: list of the warehouses by the city ref.
    For example:
    Description                                         Ref
    Відділення №1: вул. Маршала Малиновського, 114      0d545f59-e1c2-11e3-8c4a-0050568002cf
    """
    return Context(AddressGeneralGetWarehouses()).execute(request)


@require_POST
@csrf_exempt
def address_get_streets(request):
    """
    Return Nova Poshta API data of the streets.
    Url: /order/get-streets/
    Body: city_ref=db5c88f0-391c-11dd-90d9-001a92567626
          street=ман

    :return: list of the streets by the city ref and a street.
    For example:
    StreetsType     Description     Ref
    вул.            Маневрова       287eb72d-9b81-11dd-9f19-001d92f78697
    """
    return Context(AddressGetStreets()).execute(request)
