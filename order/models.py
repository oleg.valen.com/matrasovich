import json

from django.db import models
from django.http import JsonResponse
from django.utils import timezone

from product.models import Product, Attribute


# Create your models here.
class Order(models.Model):
    name = models.CharField(max_length=128)
    phone = models.CharField(max_length=128)
    sum = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return '#{}, {}'.format(self.pk, self.created.strftime('%d:%m:%Y %H:%M:%S'))

    def save(self, *args, **kwargs):
        self.updated = timezone.now()
        super().save(*args, **kwargs)

    @staticmethod
    def order(request):
        """
        Creates order and order items.

        :param request: request
        :return: if success, returns data of a created order
        """
        try:
            params = json.loads(request.body.decode())
            order_sum = sum(i['amount'] * i['price'] for i in params['items'])
            order = Order.objects.create(name=params['name'], phone=params['phone'], sum=order_sum)
            OrderItem.objects.bulk_create(
                [OrderItem(order=order, product_id=i['product_id'], attribute_id=i['attribute_id'], amount=i['amount'],
                           price=i['price']) for i in params['items']]
            )
            data = {'id': order.id, 'created': order.created.strftime('%d.%m.%Y %H:%M'), 'sum': order_sum}
            return JsonResponse({'success': True, 'data': data})

        except KeyError as e:
            return JsonResponse({'success': False, 'error': 'Mapping key not found ({}).'.format(e.args[0])})
        except Exception as e:
            return JsonResponse({'success': False, 'error': str(e)})


class OrderItem(models.Model):
    amount = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    price = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    order = models.ForeignKey(Order, related_name='items', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='+', null=True, on_delete=models.SET_NULL)
    attribute = models.ForeignKey(Attribute, related_name='+', null=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ['pk']

    def __str__(self):
        return '{}: {} ({})'.format(self.pk, self.product.name, self.attribute.name)
