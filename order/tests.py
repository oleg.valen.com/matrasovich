import json

from django.test import TestCase, RequestFactory

# Create your tests here.
from django.urls import reverse

from attribute.models import CategoryAttribute, NOINDEX_NOFOLLOW, INDEX_FOLLOW
from category.models import Category
from product.models import Product
from order.models import Order, OrderItem
from order.views import address_get_cities, address_general_get_warehouses, address_get_streets, make_order


class OrderMixin:
    fixtures = ['attribute', 'category', 'product']

    def setUp(self):
        self.factory = RequestFactory()
        self.category = Category.objects.get(pk=1)
        self.products = Product.objects.filter(pk__in=[1, 2])
        self.category_attribute = CategoryAttribute.objects.get(pk=2)


class OrderTest(OrderMixin, TestCase):

    def test_make_order(self):
        url = reverse('order:make_order')
        data = {
            'name': 'Иванов И.И.',
            'phone': '06745678912',
            'items': [
                {
                    'product_id': 1,
                    'attribute_id': 4,
                    'amount': 2,
                    'price': 1464.00
                },
                {
                    'product_id': 1,
                    'attribute_id': 5,
                    'amount': 3,
                    'price': 1484.00
                }
            ]
        }
        request = self.factory.post(url, data=data, content_type='application/json')
        response = make_order(request)
        self.assertTrue(json.loads(response.content.decode())['success'])
        self.assertQuerysetEqual(Order.objects.all(), [(1, 7380.00)], transform=lambda order: (order.pk, order.sum))
        self.assertQuerysetEqual(Order.objects.all()[0].items.all(), [1, 2], transform=lambda item: item.pk)

    def test_address_get_cities(self):
        url = reverse('order:address_get_cities')
        data = {'city': 'дне'}
        request = self.factory.post(url, data=data, content_type='application/json')
        response = address_get_cities(request)
        self.assertTrue(json.loads(response.content.decode())['success'])
        self.assertTrue(len(json.loads(response.content.decode())['data']) > 0)

    def test_address_general_get_warehouses(self):
        url = reverse('order:address_general_get_warehouses')
        data = {'city_ref': 'db5c88f0-391c-11dd-90d9-001a92567626'}
        request = self.factory.post(url, data=data, content_type='application/json')
        response = address_general_get_warehouses(request)
        self.assertTrue(json.loads(response.content.decode())['success'])
        self.assertTrue(len(json.loads(response.content.decode())['data']) > 0)

    def test_address_get_streets(self):
        url = reverse('order:address_get_streets')
        data = {'city_ref': 'db5c88f0-391c-11dd-90d9-001a92567626', 'street': 'ман'}
        request = self.factory.post(url, data=data, content_type='application/json')
        response = address_get_streets(request)
        self.assertTrue(json.loads(response.content.decode())['success'])
        self.assertTrue(len(json.loads(response.content.decode())['data']) > 0)
