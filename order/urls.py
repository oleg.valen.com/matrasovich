from django.urls import path
from . import views

app_name = 'order'
urlpatterns = [
    path('order/get-cities/', views.address_get_cities, name='address_get_cities'),
    path('order/get-warehouses/', views.address_general_get_warehouses, name='address_general_get_warehouses'),
    path('order/get-streets/', views.address_get_streets, name='address_get_streets'),
    path('order/make-order/', views.make_order, name='make_order'),
]
