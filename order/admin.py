from django.contrib import admin

from order.models import Order, OrderItem


# Register your models here.
class OrderItemInline(admin.TabularInline):
    model = OrderItem
    fields = ('product', 'attribute', 'amount', 'price')


class OrderAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
    list_display = ('name', 'phone', 'sum', 'created')
    readonly_fields = ('created', 'updated')
    inlines = [OrderItemInline]


admin.site.register(Order, OrderAdmin)
